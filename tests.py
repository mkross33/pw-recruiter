# basic unit tests for the recruiting module
# 1 command line argument: API Key

import sqlite3
import sys
from utils import get_nations, filter_noobs, personalize


def main():
    api_key = sys.argv[1]
    message = 'Hi [[ruler]] of [[nation]]'
    test_contact = {'leader': 'Mikey', 'nation': 'The Reach'}

    # Test alliance filtering
    api_data = get_nations(api_key)
    if not api_data[0]['allianceid'] == 0:
        raise SystemExit('API alliance filtering failed')

    # Test Personalization
    personalized_msg = personalize(message, test_contact)
    if not 'Mikey' and 'The Reach' in personalized_msg:
        raise SystemExit('Message personalization failed')

    # Test contacts filtering
    conn = sqlite3.connect('logs.db')
    test_filtering(conn, api_data[:3])

    print('No Problems!')


def test_filtering(conn, contacts):
    filtered_nation = contacts[0]
    c = conn.cursor()
    c.execute('''INSERT INTO logs(nation_id) VALUES(?)''', (filtered_nation['nationid'],))
    filtered_contacts = filter_noobs(contacts, conn)
    if filtered_nation in filtered_contacts:
        raise SystemExit('Contact filtering failed')


if __name__ == '__main__':
    main()


